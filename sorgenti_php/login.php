

<?php
session_start();
    
if(!empty($_SESSION['user'])){
    //header("Location: menu.php");
   if(!empty($_SESSION['admin'])) {
      // header("Location = amministrazione.php");
     echo "<script language='javascript'>window.location = 'amministrazione.php';</script>";
   }else{
       //header("Location = ordine.php");
     echo "<script language='javascript'>window.location = 'ordine.php';</script>";
    }
}
require_once 'header.php';
require_once "classi/utenti.php";
$utente = new utente($db);
$utente->server_request();

?>

<html>
  <head>
  
      <title>FoodExpress.login</title>
	
  
  </head>
	<body>
		
	<div class="gtco-loader"></div>
	<div id="page">

	<nav class="gtco-nav" role="navigation">
	<div class="gtco-container">
	<div class="row">
	<div class="col-sm-4 col-xs-12">
	<div id="gtco-logo"><a href="login.php">FoodExpress <em>.</em></a></div>
	</div>
	<div class="col-xs-8 text-right menu-1">
	<ul>
	<li class="btn-cta"><a href="registrazione.php"><span>Registrati</span></a></li>
	</ul>	
	</div>
	</div>
	</div>
	</nav>
	
	<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
		<div class="row">
		<div class="col-md-12 col-md-offset-0 text-left">
		<div class="row row-mt-15em">
		<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                <span class="intro-text-small">Benvenuti in </span>
          	<h1 class="cursive-font">FoodExpress</h1>	
		</div>
		<div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
		<div class="form-wrap">
		<div class="tab">
		<div class="tab-content">
		
                <h1 class="cursive-font">Accedi</h1>
                <p>oppure</p><a href="registrazione.php">Registrati</a>
                <form method="post" id="log_in">
		   <input type="hidden" name="h_azione" id="h_azione" value = "accedi"/>
                   <div class="row form-group">
		   <div class="col-md-12">
                   <br><label>Mail</label>
                   <input name="mail" id="mail" class="form-control" type="text">
		   </div>	
                   </div>
		   <div class="row form-group">
		   <div class="col-md-12">
		   <label>Password</label>
		   <input name="password" id="password" class="form-control" type="password">
		   </div>
		   </div>
                   <div class="row form-group">
	           <div class="col-md-12">
                       <input type="button" onclick="control_log_in()" class="btn btn-primary btn-block" value="Accedi">
	           </div>
	           </div>
                   <h2 style="color:orange"><?php echo $utente->error_message;?></h2> 
	        </form>	
	        </div>
                </div>
	        </div>
		
		</div>
		</div>
		</div>
		</div>
		</div>
	</header>
        </div>
        <script>
        
        function control_log_in(){
            var error = "";
            
            var mail = document.getElementById("mail").value;
                if (mail === "")
                   error += "inserire la mail\n";

            var password = document.getElementById("password").value;
                if (password === "")
                    error += "inserire la password\n";
             
            if (error == "")
                    document.getElementById("log_in").submit();
                else
                    alert(error);
         }
            
        </script>
        
        <script src="js/jquery.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
        <script src="js/main.js"></script>

        </body>
   </html>

   