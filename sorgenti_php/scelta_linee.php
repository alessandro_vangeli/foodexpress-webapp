<?php
session_start();

require_once 'header.php';
require_once "classi/utenti.php";
require_once "classi/admin.php";
$utente = new utente($db);
$utente->controlla_sessione();
$admin = new admin($db);
$admin->controlla_sessione();
$admin->server_request();
?>



<html>
    <head>

        
        <title>FoodExpress.sceltalinee</title>

        
        <link rel="stylesheet" type="text/css" href="css/menu_nav.css">
        <script src="js/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <div class="gtco-loader"></div>
        <div id="page">

            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div id="gtco-logo"><a href="login.php">FoodExpress <em>.</em></a></div>
                        </div>
                        <div class="col-xs-8 text-right menu-1">
                            <ul>
                                <li><a href="menu.php">Menu</a></li>
                                <li><a class="active" href="amministrazione.php">Amministrazione</a></li>
                                <li class="btn-cta"><a href="logout.php"><span>Logout</span></a></li>
                            </ul>	
                        </div>
                    </div>
                </div>
            </nav>

            <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg);height: 350px">
                <div class="overlay"></div>
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 text-center">

                            <div class="row row-mt-1em">
                                <div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
                                    <span class="intro-text-small">Benvenuto nell'area di</span>
                                    <h1 class="cursive-font">amministrazione</h1>	
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </header>

            <div class="menu_nav">
                <ul>
                    <li><a  href="amministrazione.php">scelta menù</a></li>
                    <li><a class="active" href="scelta_linee.php">scelta linee di preparazione</a></li>
                    <li><a  href="consegne.php">gestisci consegne</a></li>
                </ul>
            </div> 
            
            <div class="menu_nav">
                <ul>
                   <li><h3>Attenzione!Se è gia stato salvato un numero di linee, un nuovo inserimento cancellerà quello precedente! </h3></li>
                </ul>
            </div>
            
            <?php
               if ($admin->messaggio_conferma != "") {
                   $admin->stampa_conferma();
            }
            ?>
          

            <div class="gtco-section">
                <div class="gtco-container">
                    <div class="row">
                        <div class="row form-group">
                        <div class="col-md-12"> 
                        
                        <h3>Immetti il numero di linee di oggi</h3>
                        <form name="scelta_linee" id="scelta_linee"  method="post" action="scelta_linee.php" >    
                            <input id="num_linee" name="num_linee" placeholder="numero di linee" class="form-control" type="number" style="width:400px;" ><br><br>   
                            <input type="hidden" name="h_azione" value="salva_linee">
                            <input type="button" onclick="control_linee()" value="conferma linee" >
                        </form>
                        </div>  
                        </div>
                    </div>
                </div>
            </div>

            <script>
            
            function control_linee(){
            
                if(document.getElementById("num_linee").value <= '0')
                  alert("inserire un numero maggiore di 0");
                else document.getElementById("scelta_linee").submit();
            
            }
            </script>

        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>


