<?php
session_start();

require_once 'header.php';
require_once "classi/cliente.php";
require_once "classi/utenti.php";
$utente = new utente($db);
$utente->controlla_sessione();//var_dump($_SESSION['cliente']);
$cliente = new cliente($db);
$cliente->server_request();

header("Refresh: 15; url=conferma_automatica.php");
?>

<html>
    <head>
        
        <title>FoodExpress.attesa_conferma</title>
        
        <link rel="stylesheet" href="css/table.css">
    </head>
    
    <body>
        <?php
        $messaggio_consegna = $cliente->orario_consegna();
        ?>
        <form id="attesa_conferma" method="post" action="ordine.php">
        
            <div class="container_conferma">      
            <h3><?php echo $messaggio_consegna ?></h3>
            
            <h3>Hai 10 secondi di tempo per confermare o rifiutare l'ordine, altrimenti verrà confermato automaticamente</h3>
            <h2 style="margin-left: 350px;" >Confermi l'ordine?</h2>
            
            <input type="hidden" name="h_azione" value="attesa_conferma">
            <input type="submit" class="form-control" name="confermato" value="SI">
            <input type="submit" class="form-control" name="rifiutato" value="NO">
            </div>
        
        </form>
        
        
    </body>
    
</html>
