
<?php
session_start();

require_once 'header.php';
require_once "classi/utenti.php";
$utente = new utente($db);
$utente->server_request();

?>


<html>
    <head>
       
        <title>FoodExpress.registrazione</title>

  
    </head>
    <body>
        <div class="gtco-loader"></div>
        <div id="page">

            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div id="gtco-logo"><a href="login.php">FoodExpress <em>.</em></a></div>
                        </div>
                        <div class="col-xs-8 text-right menu-1">
                            <ul>
                                <li class="btn-cta"><a href="registrazione.php"><span>Registrazione</span></a></li>
                            </ul>	
                        </div>
                    </div>
                </div>
            </nav>
            <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg);height: 250px">
                <div class="overlay"></div>
            </header>

            <div class="gtco-section">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 animate-box">
                                <h3>Registrazione</h3>
                                <form method="post" id="form_utente_signup">
                                    <input type="hidden" name="h_azione" id="h_azione" value = "registrati"/>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="nome" name="nome" class="form-control" placeholder="nome" type="text" value="<?= empty(filter_input(INPUT_POST, 'nome')) ? "" : filter_input(INPUT_POST, 'nome') ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="cognome" name="cognome" class="form-control" placeholder="cognome" type="text" value="<?= empty(filter_input(INPUT_POST, 'cognome')) ? "" : filter_input(INPUT_POST, 'cognome') ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="mail" name="mail" class="form-control" placeholder="mail" type="text" value="<?= empty(filter_input(INPUT_POST, 'mail')) ? "" : filter_input(INPUT_POST, 'mail') ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="password" name="password" class="form-control" placeholder="password" type="password">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="n_telefono" name="n_telefono" class="form-control" placeholder="n&deg; telefono" type="text" value="<?= empty(filter_input(INPUT_POST, 'n_telefono')) ? "" : filter_input(INPUT_POST, 'n_telefono') ?>">    
                                        </div>
                                    </div>
                                    <input type="hidden" id="indirizzo_fiscale" name="indirizzo_fiscale" class="form-control"> 
                                    <input type="hidden" id="cod_fiscale" name="cod_fiscale" class="form-control">
                                    <input type="hidden" id="partita_iva" name="partita_iva" class="form-control">
                                    <div class="form-group">
                                        <input type="button" value="registrati" onclick="control_utente_signup()" class="btn btn-primary">
                                    </div>
                                    <h2 style="color:red;"><?php echo $utente->error_message; ?></h2> 
                                </form>		
                            </div>
                            <div class="col-md-5 col-md-push-1 animate-box">
                                <div class="gtco-contact-info">
                                    <br><p>Compilare i seguenti campi se si è interessati alla fattura degli ordini:</p> 
                                    <input type="checkbox" name="azienda_c" id="azienda_c" onclick="campi_opzionali()">voglio la fattura<br><br>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="indirizzo_fiscale_coldx" class="form-control" placeholder="sede legale" type="text">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="cod_fiscale_coldx" class="form-control" placeholder="codice fiscale" type="text">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input id="partita_iva_coldx" class="form-control" placeholder="partita iva" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <script>
            campi_opzionali();

            function campi_opzionali() {

                if (document.getElementById("azienda_c").checked == true) {
                    document.getElementById("indirizzo_fiscale_coldx").disabled = false;
                    document.getElementById("cod_fiscale_coldx").disabled = false;
                    document.getElementById("partita_iva_coldx").disabled = false;
                } else {
                    document.getElementById("indirizzo_fiscale_coldx").disabled = true;
                    document.getElementById("cod_fiscale_coldx").disabled = true;
                    document.getElementById("partita_iva_coldx").disabled = true;
                    document.getElementById("indirizzo_fiscale_coldx").value = "";
                    document.getElementById("cod_fiscale_coldx").value = "";
                    document.getElementById("partita_iva_coldx").value = "";
                }
            }

            //controllo campi OBBLIGATORI!!
            function control_utente_signup() {

                document.getElementById("indirizzo_fiscale").value = document.getElementById("indirizzo_fiscale_coldx").value;
                document.getElementById("cod_fiscale").value = document.getElementById("cod_fiscale_coldx").value;
                document.getElementById("partita_iva").value = document.getElementById("partita_iva_coldx").value;

                var error = "";
                var nome = document.getElementById("nome").value;
                if (nome === "")
                    error += "inserire il nome\n";

                var cognome = document.getElementById("cognome").value;
                if (cognome === "")
                    error += "inserire il cognome\n";

                var mail = document.getElementById("mail").value;
                if (mail === "")
                    error += "inserire la mail\n";

                var password = document.getElementById("password").value;
                if (password === "")
                    error += "inserire la password\n";

                var n_telefono = document.getElementById("n_telefono").value;
                if (n_telefono === "")
                    error += "inserire il numero di telefono\n";

                if (document.getElementById("azienda_c").checked == true) {

                    indirizzo_fiscale = document.getElementById("indirizzo_fiscale").value;
                    if (indirizzo_fiscale == "") {
                        error += "inserire la sede legale\n";
                    }

                    cod_fiscale = document.getElementById("cod_fiscale").value;
                    partita_iva = document.getElementById("partita_iva").value;

                    if (partita_iva == "" && cod_fiscale == "") {
                        error += "inserire la partita iva o il codice fiscale\n";
                    }

                }

                if (error == "")
                    document.getElementById("form_utente_signup").submit();
                else
                    alert(error);

            }
        </script>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>