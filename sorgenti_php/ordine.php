<?php
session_start();
require_once "classi/cliente.php";
require_once "classi/utenti.php";
$utente = new utente($db);
$utente->controlla_sessione();
$cliente = new cliente($db);
$cliente->server_request();

?>



<html>
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>FoodExpress.ordine</title>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-timepicker/jquery.timepicker.js"></script>
        
        <link rel="stylesheet" type="text/css" href="js/jquery-timepicker/jquery.timepicker.css" />
        <script type="text/javascript" src="js/jquery-timepicker/lib/site.js"></script>
        <link rel="stylesheet" type="text/css" href="js/jquery-timepicker/lib/site.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/icomoon.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/menu_nav.css">
        <link rel="stylesheet" type="text/css" href="css/table.css">
        <script src="js/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <div class="gtco-loader"></div>
        <div id="page">

            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div id="gtco-logo"><a href="login.php">FoodExpress <em>.</em></a></div>
                        </div>
                        <div class="col-xs-8 text-right menu-1">
                            <ul>
                                <li><a href="menu.php">Menu</a></li>
                                <li><a class="active" href="amministrazione.php">Amministrazione</a></li>
                                <li class="btn-cta"><a href="logout.php"><span>Logout</span></a></li>
                            </ul>	
                        </div>
                    </div>
                </div>
            </nav>

            <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg);height: 350px">
                <div class="overlay"></div>
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 text-center">

                            <div class="row row-mt-1em">
                                <div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
                                    
                                    <h1 class="cursive-font">Area cliente</h1>	
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </header>
            
             <div class="menu_nav">
                <ul>
                    <li><a class="active" href="ordine.php">Esegui un nuovo ordine</a></li>
                    <li><a  href="stato_ordini.php">Verifica lo stato dei tuoi ordini </a></li>
                    <li><a  href="storico_ordini.php">Visualizza lo storico dei tuoi ordini</a></li>
                </ul>
            </div> 
            
            <?php
            if ($cliente->messaggio_conferma != "") {
                $cliente->stampa_conferma();
            }
         
            if ($cliente->error_message != "") {
                $cliente->stampa_errore();
            } 
            
            ?>
           
            <div class="gtco-section">  
                <div class="gtco-container">
                    <div class="row">
                    <table>
                    <?php
                    $cliente->apertura_locale();
                    ?>			
                    </table>
                    </div>     
                </div>
            </div>

        </div>

     
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <script src="js/moment.min.js"></script>
      
        <script src="js/main.js"></script>

    </body>
</html>

