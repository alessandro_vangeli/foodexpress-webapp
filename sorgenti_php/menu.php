

<?php
session_start();

require_once 'header.php';
require_once "classi/cliente.php";
require_once "classi/utenti.php";
$utente = new utente($db);
$utente->controlla_sessione();
$cliente = new cliente($db);

?>


<html>
    <head>
        
        <title>Foodexpress.menu</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>

        <div class="gtco-loader"></div>

        <div id="page">

        <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div id="gtco-logo"><a href="login.php">FoodExpress <em>.</em></a></div>
                        </div>
                        <div class="col-xs-8 text-right menu-1">
                            <ul>
                                <li class="active"><a href="menu.php">Menu</a></li>
                                <li><a href="amministrazione.php">Amministrazione</a></li>
                                <li><a href="ordine.php">Ordina</a></li>
                               <li class="btn-cta"><a href="logout.php"><span>Logout</span></a></li>
                            </ul>	
                        </div>
                    </div>
                </div>
            </nav>

            <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_1.jpg);height: 350px" data-stellar-background-ratio="0.5">
                <div class="overlay"></div>
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 text-center">
                            <div class="row row-mt-1em">
                                <div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
                                    <h1 class="cursive-font">Prova il menu del giorno!</h1>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            <div class="gtco-section">
                <div class="gtco-container">
                    
                    <div class="row">
                    <?php
                       $cliente->apertura_locale();
                    ?>			
                    </div>
                </div>
            </div>
       
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>

