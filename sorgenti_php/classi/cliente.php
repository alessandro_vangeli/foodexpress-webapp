

  <?php
    require_once './config.php';

    class cliente {
    
    public $id_utente;
    public $id_ordine;
    public $messaggio_conferma;
    public $error_message;
    public $distanza;
    private $db;

    function __construct($database) {
        $this->db = $database;
        $this->error_message = "";
        $this->messaggio_conferma = "";
        $this->id_utente = $_SESSION['user']; 
        $this->id_ordine = "";
        $this->distanza = "";
    }
    
    public function server_request() {

        if (!empty(filter_input(INPUT_POST, 'h_azione'))) { 
            if(filter_input(INPUT_POST, 'h_azione') == "salva_ordine") {
                $this->controllo_ordine();
            }
            if (filter_input(INPUT_POST, 'h_azione') == "conferma_automatica"){
                $this->conferma_ordine();
            }
            if (filter_input(INPUT_POST, 'h_azione') == "attesa_conferma") {
                if(filter_input(INPUT_POST, 'confermato') == 'SI')
                    $this->conferma_ordine();
                if(filter_input(INPUT_POST, 'rifiutato') == 'NO')
                    $this->rifiuta_ordine();    
            }
        }
    }
    
    private function controllo_ordine() {
        
        $piatti_minimi = $this->controlla_piatti_minimi();
        $num = 0;
                
        for($pos = 0; $pos < count($_POST['piatti_ordine']); $pos++){
            $id_piatto = $_POST['piatti_ordine'][$pos];//per ogni piatto ordinato
            $quant = $_POST["quantita_".$id_piatto];//controllo la quantità
            $num += $quant; //sommo le quantità
        }
            if( $num >= $piatti_minimi){ //se la quantità totale di piatti ordinati è almeno il numero richiesto dal ristoratore
                   $this->salva_ordine();//procedo con il salvataggio dell'ordine
                   echo "<script language='javascript'>window.location = 'attesa_conferma.php';</script>";
            }else{ 
                  $this->error_message = "Attenzione: per questa area geografica accettiamo ordini con almeno ".$piatti_minimi." piatti !";
           }       //altrimenti stampo il messaggio di errore
    }
    
    public function stampa_errore() {
      ?>
        <div class="menu_nav">
            <ul>
                <li><h3 style="color: orange;"><?php echo $this->error_message ?></h3></li>
            </ul>
        </div>
    <?php
        
    }
    
    public function controlla_piatti_minimi() {
    
        $this->distanza = filter_input(INPUT_POST, 'distanza');
        $_SESSION['cliente']['distanza'] = $this->distanza;
        $sql =  "SELECT piatti_minimi FROM aree_consegna WHERE distanza_max = (SELECT MIN(distanza_max) FROM  aree_consegna WHERE distanza_max >= $this->distanza)";
        $result = $this->db->query($sql);//seleziono i piatti minimi in base alla distanza del domicilio dal ristorante
        $row = $result->fetch();
        $piatti_minimi = $row['piatti_minimi'];
        
        return $piatti_minimi;
        
    }    
    
    public function orario_consegna() {
        
        $this->id_ordine = $_SESSION['cliente']['id_ordine'];
           $orari = "SELECT orario_richiesto, orario_previsto FROM ordine WHERE id_ordine = $this->id_ordine";
           $result = $this->db->query($orari);//seleziono l'orario richiesto e quello previsto di evasione dell'ordine
           $row = $result->fetch();
        $orario_richiesto = $row['orario_richiesto'];
        $orario_previsto = $row['orario_previsto'];
        $distanza = $_SESSION['cliente']['distanza'];
           $tempo = "SELECT tempo_consegna FROM aree_consegna WHERE distanza_max = (SELECT MIN(distanza_max) FROM  aree_consegna WHERE distanza_max >= $distanza)";
           $result2 = $this->db->query($tempo);//seleziono il tempo di consegna per quell'area geografica
           $row2 = $result2->fetch();
        $tempo_consegna = $row2['tempo_consegna'];
        $orario_richiesto = new DateTime($orario_richiesto);//codice per formattare l'orario
        $orario_consegna = new DateTime($orario_previsto);  //codice per formattare l'orario     
        $orario_consegna->modify("+{$tempo_consegna} minutes"); //sommo l'orario previsto con quello di consegna
        $orario_consegna = date_format($orario_consegna, 'H:i');//codice per formattare l'orario
        $orario_richiesto = date_format($orario_richiesto, 'H:i');//codice per formattare l'orario
        $messaggio_consegna = "";
          if($orario_consegna <= $orario_richiesto) { //se l'orario di consegna previsto avviene entro quello richiesto dal cliente
              $messaggio_consegna = "Il tuo ordine sarà consegnato entro l'orario da te richiesto";//stampo l'opportuno messaggio
          }else{ //altrimenti stampo l'avviso di ritardo
              $messaggio_consegna = "Siamo spiacenti ma il tuo ordine sarà consegnato circa alle ".$orario_consegna." , cioè dopo l'orario da te richiesto";
          }
        
          return $messaggio_consegna;
          
    }

    private function salva_ordine() {
        
        $pulisci_code = "DELETE FROM ordine WHERE data <> CURRENT_DATE";/*funziona solo ad inizio giornata */
        $this->db->query($pulisci_code);
        $indirizzo = $_POST["indirizzo"];
        $orario = $_POST["orario"];
        $sql = "INSERT INTO ordine ( id_utente, indirizzo_consegna, orario_richiesto, conferma, data) VALUES ( '".$this->id_utente."','$indirizzo', '$orario', 0, CURRENT_DATE)";
        $this->db->query($sql);// viene inserito l'ordine nella tabella ordine
        $this->id_ordine = $this->db->lastInsertId();
        $_SESSION['cliente']['id_ordine']= $this->id_ordine;
        
        for($pos = 0; $pos < count($_POST['piatti_ordine']); $pos++){//scorre gli 'id' dei piatti scelti tramite checkbox in un array della variabile POST
           
           $id_piatto = $_POST['piatti_ordine'][$pos];//per ogni 'id' del piatto recupero
           $quant = $_POST["quantita_".$id_piatto]; // la quantità richiesta
           $proc = "CALL assegna_linea_piatto('". $this->id_ordine."','$id_piatto','$quant')"; //chiama la procedura per assegnare il piatto alla prima linea libera
           $this->db->query($proc);
        }
        $proc2 = "CALL orario_previsto('". $this->id_ordine."')"; //alla fine calcola l'orario previsto in cui l'ordine verrà evaso
        $this->db->query($proc2);
           
    }
    
    
    public function conferma_ordine() {
        
        $this->id_ordine = $_SESSION['cliente']['id_ordine'];
        $this->messaggio_conferma = "Ordine confermato!";
        $sql = "UPDATE ordine SET conferma = 1 WHERE id_ordine = $this->id_ordine";//confermo l'ordine impostando a '1' l'attributo
        $this->db->query($sql);
       
    }
    
    private function rifiuta_ordine() {
        
        $this->id_ordine = $_SESSION['cliente']['id_ordine'];
        $this->messaggio_conferma = "Ordine rifiutato! Adesso potrai comporre un altro ordine!";
        $sql = "DELETE FROM ordine WHERE id_ordine = $this->id_ordine" ;//elimino l'ordine rifiutato
        $this->db->query($sql);
     
    }
    
    public function stampa_conferma() {
        
        ?>
            <div class="menu_nav">
                <ul>
                   <li><h3 style="color:orange"><?php echo $this->messaggio_conferma; ?> </h3></li>
                </ul>
            </div>
        <?php
    }
    
    public function stato_ordini() {
        
        $sql = "SELECT orario_richiesto, TIMEDIFF(orario_previsto, CURRENT_TIME) AS tempo_mancante FROM ordine WHERE id_utente = $this->id_utente AND ordine.data = CURRENT_DATE AND TIMEDIFF(orario_previsto, CURRENT_TIME)>=0";
        $result = $this->db->query($sql);//sleziono l'orario richiesto e il tempo mancante all'evasione per ogni ordine effettuato dal cliente se il tempo mancante è maggiore di '0', cioè se non è stato ancora evaso 
        $orari = $result->fetchAll();
        ?>
        
        <h3>Stato degli ordini</h3>                      
        <table class="table_style" style="width:700px"><?php
        $num_ordine = 1;
          ?><tr >
            <th >
                <p>n° ordine</p>
            </th>
            <th >
                <p>orario richiesto</p>
            </th>
            <th >
                <p>l'ordine sarà evaso tra: </p>
            </th>
            </tr><?php
        foreach($orari as $stato){
            $orario_richiesto = new DateTime($stato['orario_richiesto']);//codice per formattare l'orario
            $tempo_mancante = new DateTime($stato['tempo_mancante']);//codice per formattare l'orario
            $orario_richiesto = date_format($orario_richiesto, 'H:i');//codice per formattare l'orario
            $intervallo_ora_1 = new DateTime("00:00:00");
            $intervallo_ora_2 = new DateTime("00:59:59");
            if($tempo_mancante <= $intervallo_ora_2 && $tempo_mancante > $intervallo_ora_1) {
               $tempo_mancante = date_format($tempo_mancante, 'i')." minuti";//se il tempo mancante è masimo di un'ora stampo i minuti
            }else{                                                           //altrimenti le ore
                $tempo_mancante = date_format($tempo_mancante, 'H:i')." ore";
            }
                
              ?>
                <tr >
                <td>
                    <?php echo $num_ordine?>
                </td>
                <td >
                    <?php echo $orario_richiesto?>
                </td>
                <td >
                    <?php echo $tempo_mancante?>
                </td>
                </tr>
         <?php           
            $num_ordine++;
        }
      ?></table>
       <?php
          
    }
    
    public function storico_ordini(){
        
        
        $sql = "SELECT data, num_ordini, spesa_totale FROM storico_ordini WHERE id_utente = $this->id_utente ";
        $result = $this->db->query($sql);//sleziono lo storico degli ordini per l'utente
        $storico = $result->fetchAll();
        ?>
        
        <h3>Storico ordini</h3>                      
        <table class="table_style" style="width:700px"><?php
        
          ?><tr >
            <th >
                <p>data</p>
            </th>
            <th >
                <p>numero di ordini</p>
            </th>
            <th >
                <p>spesa totale </p>
            </th>
            </tr><?php
           foreach($storico as $row){
                
              ?>
                <tr >
                <td>
                    <?php echo $row['data']?>
                </td>
                <td >
                    <?php echo $row['num_ordini']?>
                </td>
                <td >
                    <?php echo $row['spesa_totale']." €"?>
                </td>
                </tr>
         <?php           
            
            }
      ?></table>
       <?php
          
    
        
    }


    public function validita_stato_ordini(){
        
        $sql = "SELECT orario_richiesto, TIMEDIFF(orario_previsto, CURRENT_TIME) AS tempo_mancante FROM ordine WHERE id_utente = $this->id_utente AND ordine.data = CURRENT_DATE AND TIMEDIFF(orario_previsto, CURRENT_TIME)>=0";
        $result = $this->db->query($sql);
       
        if ($result->rowCount() > 0) {//se esiste almeno un ordine in attesa di essere evaso 
               $this->stato_ordini(); //allora stampo i tempi mancanti
        } else {   //altrimenti stampo il messaggio di avviso
          ?><div class="col-md-8 col-md-offset-2 text-center gtco-heading">
             <h2 class="cursive-font primary-color">Oggi non hai ancora effettuato ordini, oppure sono già stati evasi!</h2>
            </div><?php 
        }
    }
    
    public function validita_storico_ordini(){
        
        $sql = "SELECT * FROM storico_ordini WHERE id_utente = $this->id_utente";
        $result = $this->db->query($sql);
       
        if ($result->rowCount() > 0) {//se esiste almeno un ordine effettuato in una data qualsiasi
               $this->storico_ordini();//stampo la tabella con lo storico degli ordini
        } else { //altrimenti stampo il messaggio di avviso
          ?><div class="col-md-8 col-md-offset-2 text-center gtco-heading">
             <h2 class="cursive-font primary-color">Non hai mai effettuato ordini sul nostro sito!</h2>
            </div><?php 
        }
    }
    
    public function apertura_locale(){
        
        $sql = "SELECT * FROM disponibilita_piatto JOIN linee_preparazione ON disponibilita_piatto.data = data_linea WHERE data_linea = CURRENT_DATE" ;
        $result = $this->db->query($sql);//se il ristoratore ha scelto sia le linee che il menù
         
        if ($result->rowCount() > 0) {//se il ristoratore ha scelto sia le linee che il menù
           if(($_SERVER['PHP_SELF']) == '/foodapp/menu.php')   //se mi trovo nella pagina menù stampo il menù
               $this->stampa_menu();
           if(($_SERVER['PHP_SELF']) == '/foodapp/ordine.php') //se mi trovo nella pagina per ordinare, stampo la lista dei piatti 
               $this->stampa_lista_cliente();
        } else {
            $this->stampa_chiusura();
        }
    }
    
    
    private function stampa_chiusura() {
        
        ?>
         <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
             <h2 class="cursive-font primary-color">Locale ancora chiuso!</h2>
         </div> 
        
        <?php
    }
    
    public function stampa_menu() {

        $sql = "SELECT * FROM piatto NATURAL JOIN disponibilita_piatto WHERE disponibilita_piatto.data = CURRENT_DATE";
        $result = $this->db->query($sql);
        $rs_piatti = $result->fetchAll();

        foreach ($rs_piatti as $row) {
        ?>
        <div class="col-lg-3 col-md-3 col-sm-3">
        <a href="images/<?= $row["foto"] ?>" class="fh5co-card-item image-popup">
            <figure>
            <div class="overlay"><i class="ti-plus"></i></div>
            <div class="ImgMenu">
               <img src="images/<?= $row["foto"] ?>" alt="Image" class="img-responsive">
            </div>
            </figure>
            <div class="fh5co-text">
            <h2><?= $row["titolo"] ?></h2>
            <p><?= $row["descrizione"] ?></p>
              <h3><span class="price cursive-font"><?= $row["prezzo"]."€" ?></span></h3>
            </div>
        </a>
        </div>

        <?php
            
        }
    }
    
     
    public function stampa_lista_cliente() {
    ?>
    <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
        <h2 class="cursive-font primary-color">Ordina i tuoi piatti preferiti!</h2>
    </div>    
    <form id="lista_piatti_ordine" method="post" >
        <input type="hidden" name="h_azione" value="salva_ordine">
        <?php
        $sql = "SELECT * FROM piatto NATURAL JOIN disponibilita_piatto WHERE disponibilita_piatto.data = CURRENT_DATE";
        $result = $this->db->query($sql);//seleziono il menù del giorno
        $rs_ordine = $result->fetchAll();
        $max_col = $result->rowCount();//codice per stampare i piatti in una tabella
        $col_count = range(1,$max_col);//codice per stampare i piatti in una tabella
        $rows = array_chunk($col_count,5);//codice per stampare i piatti in una tabella
     
        ?> <table class="table_style2"> <?php
        $ult_col=0;//codice per stampare i piatti in una tabella
        for($row=0 ;$row<count($rows);$row++){//codice per stampare i piatti in una tabella
        ?><tr><?php
           for($col=$ult_col;$col<count($rows[$row])+$ult_col;$col++){//codice per stampare i piatti in una tabella
           ?><td style="border: 1px solid #ddd;">
               <table class="table_style2">
               <tr>    
               <div class="imgContainer">
                   <img src="images/<?= $rs_ordine[$col]["foto"] ?>" alt="Image">
               </div></tr><tr> 
                <input id="piatto_<?php echo $rs_ordine[$col]["id_piatto"]; ?>" name="piatti_ordine[]" type="checkbox" onclick="abilita_piatto(this);" value="<?= $rs_ordine[$col]["id_piatto"] ?>" /><p><?= $rs_ordine[$col]["titolo"] ?></p>
                </tr>
                <tr><p>Inserire quantità:</p>
                <select id="quantita_<?php echo $rs_ordine[$col]["id_piatto"]; ?>" name="quantita_<?php echo $rs_ordine[$col]["id_piatto"]; ?>" disabled />
          <?php for($disp=0;$disp<=$rs_ordine[$col]["disponibilita"];$disp++) { //faccio scegliere la quantità se il piatto è disponibile
                 if($rs_ordine[$col]["disponibilita"]==0) { 
                    $disp = "non disponibile";   
                 }  ?>                
                <option value="<?php echo $disp ?>" > <?php echo $disp ?> </option> 
          <?php } ?>
                </select>
                </tr>
                </table>
            </td>
            <?php
            }
             $ult_col = count($rows[$row])+$ult_col;//codice per stampare i piatti in una tabella
        ?></tr><?php
        }
        ?></table><?php
        $sql1 = "SELECT MAX(distanza_max) AS distanza_max FROM aree_consegna";
        $result1 = $this->db->query($sql1);//seleziono la fascia geografica più distante che il ristoratore riesce a servire 
        $row1 = $result1->fetch();
        
        ?><br><br><br>
        <table class="table_style2" style="margin-bottom:50px"><tr><td>
                <p>Indirizzo di consegna</p><input name="indirizzo" id="indirizzo" class="form-control" type="text" style="width:400px;">
                   </td><td>
                    <p>Quanto sei distante dal nostro ristorante?</p>
                    <select name="distanza" id="distanza" class="form-control" type="number" style="width:100px;margin-left: 40px;" />                
                <?php for($dist=0;$dist<=$row1['distanza_max'];$dist+=2) { //do l'opportunità di scegliere le distanze ogni 2 km
                        $dist_original = $dist;                            //per non occupare troppo spazio
                        if($dist==0) {
                           $dist = 1;
                        }?>                
                        <option value=" <?php echo $dist ?>"> <?php echo $dist." km" ?> </option>
                        <?php 
                        if($dist == 1){
                            $dist = $dist_original;
                        }
                      } 
                      ?>    
                    </select>
                      </td>
                    </tr><tr><td>
                            <p>Orario richiesto per la consegna</p><input  name="orario" id="timeformat" class="time" type="text">
                    </td></tr>
        </table> 
        <?php if($result->rowCount() > 0) { ?> 
                 <input type="button" value="Ordina" onclick="control_ordine()" style="width:150px;height:70px;margin:auto;display:block">
        <?php } ?>
                   
    </form>

        <script>

            function abilita_piatto(me) {
                                
                if (document.getElementById("piatto_" + me.value).checked == true) {
                    document.getElementById("quantita_" + me.value).removeAttribute("disabled");
                } else {
                    document.getElementById("quantita_" + me.value).setAttribute("disabled", true);
                }
                
                 
            }
           
            
            function control_ordine() {
            
               var quant_mancante = 0;
               var error = "";
             
                var $valori = $("input[type=checkbox][name='piatti_ordine[]']:checked");
                
                if ($valori.length) {
                   var array_quantita = $valori.map(function(){return this.value;}).get();
                   for(i=0;i<array_quantita.length;i++){
                       var select = document.getElementById("quantita_"+array_quantita[i]);
                       var quantita = select.options[select.selectedIndex].value;
                       if(quantita == '0'|| quantita == 'non disponibile')
                           quant_mancante++;
                   } 
                   if(quant_mancante >1)
                       var lett = 'e';
                   else lett = 'a';
                   if(quant_mancante >0){
                    error+= "hai inserito "+quant_mancante+" quantità non valid"+lett+"\n"
                   }else{
                     error+="";  
                   } 
                } else {
                    error+="non hai selezionato nessun piatto!\n";
                }
              
                var indirizzo = document.getElementById("indirizzo").value;
                if (indirizzo === "")
                    error += "inserire l'indirizzo\n";

                var distanza = document.getElementById("distanza").value;
                if (distanza === "")
                    error += "inserire la distanza\n";

                var orario = document.getElementById("timeformat").value;
                if (orario === "")
                    error += "inserire l'orario\n";
                
                
                if(error == ""){
                   document.getElementById("lista_piatti_ordine").submit();
                }else{
                    alert(error);
                   
                }
                   

            }
            
            
            $(function() {
                $('#timeformat').timepicker({ 'step': 15 , timeFormat: 'H:i:s','disableTimeRanges': [['00:00:00', '08:00:00'],['22:00:00', '00:00:00']] });
            });

        </script>

        <?php
    }

    

}
?>
 
