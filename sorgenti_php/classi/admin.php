
  <?php
    require_once './config.php';

    class admin {

    private $db;
    public $messaggio_conferma;

    function __construct($database) {
        $this->db = $database;
        $this->messaggio_conferma = "";
    }

    public function controlla_sessione() {
    
        if (empty($_SESSION['admin']) ) {
            //header("Location = login.php");
            echo "<script language='javascript'>window.location = 'login.php';</script>";
        }
    }
    
    
    public function server_request() {

        if (!empty(filter_input(INPUT_POST, 'h_azione'))) { 
         
            if(filter_input(INPUT_POST, 'h_azione') == "salva_lista_piatti") {
                  $this->salva_piatti();
            } elseif (filter_input(INPUT_POST, 'h_azione') == "salva_linee") {
                  $this->salva_linee();
            } elseif (filter_input(INPUT_POST, 'h_azione') == "elimina_riga") {
                  $this->elimina_fascia_max();
            } elseif (filter_input(INPUT_POST, 'h_azione') == "aggiungi_riga") {
                  $this->aggiungi_fascia();
            }
                    
        } 
    }

    private function aggiungi_fascia() {
        
        $dist_max = $_POST['dist_max'];
        $piatti_min = $_POST['piatti_min'];
        $tempo_consegna = 2*$dist_max; //il tempo di consegna è calcolato come il doppio della distanza massima per quella fascia
        $sql = "INSERT INTO aree_consegna ( distanza_max, piatti_minimi, tempo_consegna) VALUES ( '$dist_max','$piatti_min','$tempo_consegna') "; 
        $this->db->query($sql);
        
    }
    private function elimina_fascia_max() {
            
        $sql = "DELETE FROM aree_consegna ORDER BY id_area DESC LIMIT 1;"; //elimina l'ultima riga della tabella aree_consegna
        $this->db->query($sql);
 
    } 
   
    public function aggiungi_bottone() {

        $sql = "SELECT * FROM aree_consegna";
        $result = $this->db->query($sql);
        if ( $result->rowCount() > 1) { //se esiste più di una riga nella tabella(e quindi una fascia) , stampa il pulsante per permettere di eliminare l'ultima
            ?> <input type="submit" value="elimina ultima fascia" > <?php
        }
      
    }
        
    public function stampa_aree_geografiche() {
    
        $sql = "SELECT * FROM aree_consegna";
        $result = $this->db->query($sql);
        $rs_aree = $result->fetchAll();
        ?> 
        <table class="table_style" style="width:700px">
            <tr style="border: 1px solid #ddd;">
            <th style="border: 1px solid #ddd;">
                <p>distanza massima</p>
            </th>
            <th style="border: 1px solid #ddd;">
                <p>numero minimo di piatti per ordine</p>
            </th>
            <th style="border: 1px solid #ddd;">
                <p>tempo di consegna(ricavato)</p>
            </th>
            </tr>
                 <?php
        foreach ($rs_aree as $row) {
            ?>
            <tr>
                <td style="border: 1px solid #ddd;">
                    <?= $row['distanza_max']." km" ?>
                </td>
                <td style="border: 1px solid #ddd;">
                    <?= $row['piatti_minimi'] ?>
                </td>
                <td style="border: 1px solid #ddd;">
                    <?= $row['tempo_consegna']." minuti" ?>
                </td>
            </tr>
            
        <?php
        }
        ?>  <tr>    
                <td style="border: 1px solid #ddd;">
                <input id="dist_max" name="dist_max" class="form-control" placeholder="inserire nuova distanza" type="number">
                </td>
                <td style="border: 1px solid #ddd;">
                <input id="ordini_min" name="piatti_min" class="form-control" placeholder="inserire numero piatti" type="number">
                </td>
                <td style="border: 1px solid #ddd;">
                <input type="submit" value="aggiungi fascia" >
                </td>
            </tr>  
                            
       </table><?php
    }
    
    private function salva_piatti() {
        
        $sql = "DELETE FROM disponibilita_piatto WHERE disponibilita_piatto.data = CURRENT_DATE"; //elimina le righe di disponibilita_piatto
        $result = $this->db->query($sql);                                                        //dove la data è quella ordierna(elimina perciò il menù del giorno 
                                                                                                 //se esiste prima di salvare quello nuovo)
        for($pos = 0; $pos < count($_POST['piatti_giorno']); $pos++){  //scorre gli 'id' dei piatti scelti tramite checkbox in un array della variabile POST
           
           $id_piatto = $_POST['piatti_giorno'][$pos];   //per ogni 'id' del piatto recupero
           $disp = $_POST["disponibilita_".$id_piatto];  //la corrispondente disponibilità 
           $prezzo = $_POST["prezzo_".$id_piatto];       //e il prezzo scelto 
           $this->messaggio_conferma = "Menu salvato!";
           
           $sql = "INSERT INTO disponibilita_piatto (id_piatto, disponibilita, prezzo, data) VALUES ('$id_piatto', '$disp','$prezzo', CURRENT_DATE)";
           $this->db->query($sql);
           
        }
    }
    
    private function salva_linee() {
        
        $sql = "DELETE FROM linee_preparazione WHERE data_linea = CURRENT_DATE"; //elimina le righe di linee_preparazione corrispondenti alla data odierna
        $result = $this->db->query($sql);                                        // nel caso esistessero già prima di salvare la nuova scelta
        
        $num_linee = filter_input(INPUT_POST, 'num_linee'); 
        $this->messaggio_conferma = "Linee salvate!";
        
        for($linea=1;$linea<=$num_linee;$linea++){
         $sql = "INSERT INTO linee_preparazione (data_linea, num_linea) VALUES (CURRENT_DATE, '$linea')";
         $this->db->query($sql);
        }
    }
    
    public function stampa_conferma() {
        
        ?>
            <div class="menu_nav">
                <ul>
                   <li><h3 style="color:orange"><?php echo $this->messaggio_conferma; ?> </h3></li>
                </ul>
            </div>
        <?php
    }

    
    
    public function stampa_lista_admin() {

        $sql = "SELECT * FROM PIATTO";
        $result = $this->db->query($sql);
        $rs_piatti = $result->fetchAll();
        $max_col = $result->rowCount();
        $col_count = range(1,$max_col); //codice per stampare i piatti in una tabella
        $rows = array_chunk($col_count,5); //codice per stampare i piatti in una tabella
        ?>
        <form name="lista_piatti_admin" id="lista_piatti_admin" method="post" >
            <input type="hidden" name="h_azione" value="salva_lista_piatti">
            <table class="table_style2" > <?php
            $ult_col=0;//codice per stampare i piatti in una tabella
            for($row=0 ;$row<count($rows);$row++){ //codice per stampare i piatti in una tabella
            ?><tr><?php
                for($col=$ult_col;$col<count($rows[$row])+$ult_col;$col++){ //codice per stampare i piatti in una tabella
                ?><td style="border: 1px solid #ddd;"> 
                    <table class="table_style2" >
                    <tr>    
                    <div class="imgContainer"> 
                        <img src="images/<?= $rs_piatti[$col]["foto"] ?>" alt="Image">
                    </div></tr><tr>
                    <input id="piatto_<?php echo $rs_piatti[$col]["id_piatto"]; ?>" name="piatti_giorno[]" type="checkbox" onclick="abilita_piatto(this);" value="<?= $rs_piatti[$col]["id_piatto"] ?>" /><p><?= $rs_piatti[$col]["titolo"] ?></p>
                    </tr><tr>
                    <input id="prezzo_<?php echo $rs_piatti[$col]["id_piatto"]; ?>" name="prezzo_<?= $rs_piatti[$col]["id_piatto"] ?>"  placeholder="prezzo" type="number" disabled />
                    </tr><tr>
                    <input id="disponibilita_<?php echo $rs_piatti[$col]["id_piatto"]; ?>" name="disponibilita_<?php echo $rs_piatti[$col]["id_piatto"]; ?>" placeholder="disponibilita" type="number" disabled />
                    </tr>
                    </table>
                 </td>
            <?php
             }
             $ult_col = count($rows[$row])+$ult_col; //codice per stampare i piatti in una tabella
        ?></tr><?php
        }
        ?></table>
            <br><br>
               <input type="button" value="salva men&ugrave;" onclick="control_lista_admin()" style="width:150px;height:70px;margin:auto;display:block">
              
        </form>

        <script>

            function abilita_piatto(me) {
                                
                if (document.getElementById("piatto_" + me.value).checked == true) {
                    document.getElementById("prezzo_" + me.value).removeAttribute("disabled");
                    document.getElementById("disponibilita_" + me.value).removeAttribute("disabled");
                } else {
                    document.getElementById("prezzo_" + me.value).value = "";
                    document.getElementById("disponibilita_" + me.value).value = "";
                    document.getElementById("prezzo_" + me.value).setAttribute("disabled",true);
                    document.getElementById("disponibilita_" + me.value).setAttribute("disabled", true);
                }

            }

            function control_lista_admin() {
              
                var prezzo_mancante = 0;
                var disp_mancante = 0;
                var error = "";
             
                var $valori = $("input[type=checkbox][name='piatti_giorno[]']:checked");
                
                if ($valori.length) {
                  var array_piatti = $valori.map(function(){return this.value;}).get();
                   for(i=0;i<array_piatti.length;i++){
                       var prezzo = document.getElementById("prezzo_"+array_piatti[i]).value;
                       var disp = document.getElementById("disponibilita_"+array_piatti[i]).value;
                       if(prezzo <= '0'|| prezzo == "")
                           prezzo_mancante++;
                       if(disp <= '0'|| disp == "")
                           disp_mancante++;
                   } 
                   if(prezzo_mancante >1)  var lett = 'i'; else lett = 'o';
                   if(disp_mancante >1)  var lett1 = 'e'; else lett1 = 'a';
                   if(prezzo_mancante >0){
                    error+= "hai inserito "+prezzo_mancante+" prezz"+lett+" non valid"+lett+"\n"
                   } 
                   if(disp_mancante >0){
                    error+= "hai inserito "+disp_mancante+" disponibilità non valid"+lett1+"\n"
                   }
                   
                } else {
                    error+="non hai selezionato nessun piatto!\n";
                }
                             
                if(error == ""){
                   document.getElementById("lista_piatti_admin").submit();
                }else{
                    alert(error);
                }
            
            
        }

        </script>

        <?php
    }

}
?>
