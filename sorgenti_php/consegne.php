<?php
session_start();

require_once 'header.php';
require_once "classi/utenti.php";
require_once "classi/admin.php";
$utente = new utente($db);
$utente->controlla_sessione();
$admin = new admin($db);
$admin->controlla_sessione();
$admin->server_request();
?>



<html>
    <head>

      
        <title>FoodExpress.consegne</title>

        <link rel="stylesheet" type="text/css" href="css/menu_nav.css">
        <link rel="stylesheet" type="text/css" href="css/table.css">
        <script src="js/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <div class="gtco-loader"></div>
        <div id="page">

            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div id="gtco-logo"><a href="login.php">FoodExpress <em>.</em></a></div>
                        </div>
                        <div class="col-xs-8 text-right menu-1">
                            <ul>
                                <li><a href="menu.php">Menu</a></li>
                                <li><a class="active" href="amministrazione.php">Amministrazione</a></li>
                                <li class="btn-cta"><a href="logout.php"><span>Logout</span></a></li>
                            </ul>	
                        </div>
                    </div>
                </div>
            </nav>

            <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg);height: 350px">
                <div class="overlay"></div>
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 text-center">

                            <div class="row row-mt-1em">
                                <div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
                                    <span class="intro-text-small">Benvenuto nell'area di</span>
                                    <h1 class="cursive-font">amministrazione</h1>	
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </header>

            <div class="menu_nav">
                <ul>
                    <li><a  href="amministrazione.php">scelta menù</a></li>
                    <li><a  href="scelta_linee.php">scelta linee di preparazione</a></li>
                    <li><a class="active" href="consegne.php">gestisci consegne</a></li>
                </ul>
            </div> 
          
            <div class="gtco-section">
                <div class="gtco-container">
                    <div class="row">
                        
                    <h3>Aggiungi o elimina le aree di consegna</h3>
                    <form id="aggiungi_riga" method="POST" action="consegne.php">
                        <input type="hidden" name="h_azione" value="aggiungi_riga">
                       
                        <?php
                            $admin->stampa_aree_geografiche();
                        ?>
                        <br>
                    </form>
                        </div>
                    <form  id="elimina_riga" method="POST">
                        <input type="hidden" name="h_azione" value="elimina_riga">
                        <?php  
                            $admin->aggiungi_bottone();
                        ?>
                    </form>    
                      
                </div>  
            </div>
               

        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
