<?php

require_once './config.php';

class utente {

    public $id_utente;
    public $utente_tipo;
    public $mail;
    public $password;
    public $nome;
    public $cognome;
    public $n_telefono;
    public $indirizzo_fiscale;
    public $cod_fiscale;
    public $partita_iva;
    public $error_message;
    private $db;

    function __construct($database) {
        $this->db = $database;
        $this->error_message = "";
    }

    public function controlla_sessione() {
        if (empty($_SESSION['user'])) {
            header("Location: login.php");
        }
    }

    public function server_request() {

        if (!empty(filter_input(INPUT_POST, 'h_azione'))) {
        
            if (filter_input(INPUT_POST, 'h_azione') == "registrati") {
                $this->registrazione_utente();
            } elseif (filter_input(INPUT_POST, 'h_azione') == "accedi") {
                $this->login_utente();
            }  
        }
    }

  
    private function login_utente() {

        $mail = filter_input(INPUT_POST, 'mail');
        $password = filter_input(INPUT_POST, 'password');
        $sql = "SELECT id_utente FROM utente WHERE mail = '$mail' AND password = '$password'";
        $result = $this->db->query($sql);

        if ($result->rowCount() > 0) {
            $row = $result->fetch(PDO::FETCH_ASSOC);
            $_SESSION['user'] = $row["id_utente"];
            $_SESSION['cliente'] = array("distanza"=>"", "id_ordine"=>"") ;
            if ($row["id_utente"] == 1) {
                $_SESSION['admin'] = 1;   
                echo "<script language='javascript'>window.location = 'amministrazione.php';</script>";
                //header("Location = amministrazione.php");
            } else {
                echo "<script language='javascript'>window.location = 'ordine.php';</script>";
                //header("Location = ordine.php");
            }
        } else {
            $this->error_message = "campi errati!";
        }
    }

    private function registrazione_utente() {

        //controlli mail esistenti
        if (!empty(filter_input(INPUT_POST, 'mail'))) {
            $mail = filter_input(INPUT_POST, 'mail');
        }
        $sql = "SELECT * FROM utente WHERE mail = '$mail'";
        $result = $this->db->query($sql);

        if ($result->rowCount() > 0) {
            $this->error_message = "mail già esistente";
        }

       

        if ($this->error_message == "") {
            $array_insert_reg = array();

            foreach ($_POST as $key => $value) {
                if (!empty(filter_input(INPUT_POST, $key))) {
                    $array_insert_reg[$key] = $value;
                }
            }
            $this->db->insert("utente", $array_insert_reg);
            //header("Location = login.php");
            echo "<script language='javascript'>window.location = 'login.php';</script>";
        }
    }
    
   
}
